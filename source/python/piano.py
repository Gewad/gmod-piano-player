from pynput.keyboard import Key, Controller
import time

# 1 = 1
# 2 = 1/2
# 3 = 1/4
# 4 = 1/8
# 5 = 1/16
# 6 = 1/32

class PianoController:
    keyboard = Controller();
    currentlyPressed = "";

    def playSequence(self, tempo, sequence):
        for instruction in sequence:
            if(len(instruction) > 1):
                self.playMultiple(instruction);
            elif(47 < ord(instruction) < 58):
                self.rest(tempo, instruction)
            elif(64 < ord(instruction) < 91):
                self.playSingle(instruction, True);
            elif(96 < ord(instruction) < 123):
                self.playSingle(instruction, False);
                
    def playSingle(self, instruction, capitalised):
        print("Playing:", instruction, capitalised);
        if(capitalised):
            print("Doing this")
            self.keyboard.press(Key.shift)  
        self.keyboard.press(instruction)
        self.currentlyPressed += instruction;
                
    def playMultiple(self, instruction):
        for element in instruction:
            self.playSingle(element)
            
    def rest(self, tempo, instruction):
        if(instruction == "1"):
            time.sleep(.25)
        elif(instruction == "2"):
            time.sleep(0.5)
            
        self.releaseAll()

    def releaseAll(self):
        print("Releasing all keys")
        for char in self.currentlyPressed:
            self.keyboard.release(char)
        self.keyboard.release(Key.shift)
        self.currentlyPressed = ""
        
        
        
piano = PianoController()
time.sleep(3)

piano.playSequence(2, ("o", "1", "t", "1", "Y", "1", "i", "1", "o", "1", "t", "1", "Y", "1", "i", "1", "y", "1"))