package ui;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import java.awt.Color;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import pianocontroller.Piano;

import java.awt.Font;
import javax.swing.JCheckBox;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;

public class Window {

	private JFrame frame;
	private JPanel songPlayer;
	private JPanel songSelector;
	
	private Piano master;
	private String[] sequences;
	
	private int startPosition = 0;
	private int selected = 0;
	
	private String selectedSong;
	private int BPM;
	private boolean repeat;
	
	JSpinner bpmSpinner;
	JCheckBox chckbxRepeat;

	public Window(Piano master) {
		this.master = master;
		initialize();
		frame.setVisible(true);
	}

	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(Color.BLACK);
		frame.setBounds(100, 100, 450, 339);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		frame.setTitle("Gewad's Piano Player");
		
		sequences = this.master.getSequences();
		
		songSelector = new JPanel();
		songPlayer = new JPanel();
		
		// Stops the song playing before exiting to prevent buttons being held down.
		WindowListener exitListener = new WindowAdapter() {
		    @Override
		    public void windowClosing(WindowEvent e) {
		        master.stop();
		        try {
					Thread.sleep(100);
				} catch(Exception er) {
					er.printStackTrace();
				}
		        System.exit(0);
		    }
		};
		frame.addWindowListener(exitListener);
		
		if(sequences.length == 0) {
			JOptionPane.showMessageDialog(frame, "In order for this program to work there must be songs in the music.txt file.\n You can find examples on the GitLab page(https://gitlab.com/Gewad/gmod-piano-player)");
			System.exit(0);
		} else {
			makeButton();
			makePlayer();
		}
	}
	
	public void makeButton() {
		songSelector.setVisible(false);
		songSelector.removeAll();
		
		songSelector.setBackground(Color.WHITE);
		songSelector.setBounds(0, 0, 190, 300);
		frame.getContentPane().add(songSelector);
		songSelector.setLayout(null);
		
		JButton btnUp = new JButton("\u001E\u25B2");
		if(startPosition == 0) {
			btnUp.setBackground(Color.LIGHT_GRAY);
		} else {
			btnUp.setBackground(Color.WHITE);
			btnUp.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					startPosition--;
					makeButton();
				}
			});
		}
		btnUp.setBounds(0, 0, 190, 20);
		songSelector.add(btnUp);
		
		JButton btnDown = new JButton("\u25BC");
		if((sequences.length + startPosition) > 13 && startPosition + 13 < sequences.length) {
			btnDown.setBackground(Color.WHITE);
			btnDown.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					startPosition++;
					makeButton();
				}
			});
		} else {
			btnDown.setBackground(Color.LIGHT_GRAY);
		}
		btnDown.setBounds(0, 280, 190, 20);
		songSelector.add(btnDown);
		
		JButton songButtons[] = new JButton[13];
		for(int i = 0; i < 13 && i < sequences.length; i++) {
			songButtons[i] = new JButton(sequences[i+startPosition]);
			if(i+startPosition == selected) {
				songButtons[i].setBackground(Color.GREEN);
			} else {
				songButtons[i].setBackground(Color.WHITE);
			}
			songButtons[i].setBounds(0, 20+i*20, 190, 20);
			songButtons[i].putClientProperty("songNumber", i);
			songButtons[i].addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					selected = (Integer) ((JButton) e.getSource()).getClientProperty("songNumber") + startPosition;
					makePlayer();
					makeButton();
				}
			});
			songSelector.add(songButtons[i]);
		}
		
		songSelector.setVisible(true);
	}
	
	public void makePlayer() {
		songPlayer.setVisible(false);
		songPlayer.removeAll();
		
		songPlayer.setBackground(Color.WHITE);
		songPlayer.setBounds(190, 0, 244, 300);
		frame.getContentPane().add(songPlayer);
		songPlayer.setLayout(null);
		
		// Get song data
		selectedSong = sequences[selected];
		String[] songData = this.master.getSequenceData(selectedSong);
		BPM = (int) Double.parseDouble(songData[1]);
		repeat = Boolean.parseBoolean(songData[2]);
		
		JLabel lblSongName = new JLabel(selectedSong);
		lblSongName.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblSongName.setHorizontalAlignment(SwingConstants.CENTER);
		lblSongName.setBounds(0, 0, 244, 30);
		songPlayer.add(lblSongName);
		
		JLabel lblBpm = new JLabel("BPM");
		lblBpm.setHorizontalAlignment(SwingConstants.CENTER);
		lblBpm.setBounds(0, 41, 244, 30);
		songPlayer.add(lblBpm);
		
		chckbxRepeat = new JCheckBox("Repeat");
		chckbxRepeat.setSelected(repeat);
		chckbxRepeat.setBackground(Color.WHITE);
		chckbxRepeat.setBounds(92, 92, 89, 23);
		songPlayer.add(chckbxRepeat);
		
		bpmSpinner = new JSpinner();
		bpmSpinner.setModel(new SpinnerNumberModel(new Integer(BPM), null, null, new Integer(1)));
		bpmSpinner.setBounds(92, 65, 61, 20);
		songPlayer.add(bpmSpinner);
		
		JButton btnSave = new JButton("Save");
		btnSave.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				makeChanges();
				master.saveChanges();
			}
		});
		btnSave.setBackground(Color.WHITE);
		btnSave.setBounds(75, 266, 89, 23);
		songPlayer.add(btnSave);
		
		JButton btnPlayStop = new JButton();
		btnPlayStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		if(master.getPlaying()) {
			btnPlayStop.setText("Stop");
			btnPlayStop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					master.stop();
					makePlayer();
				}
			});
		} else {
			btnPlayStop.setText("Play");
			btnPlayStop.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					makeChanges();
					try {
						Thread.sleep(3000);
					} catch(Exception er) {
						er.printStackTrace();
					}
					master.play(selectedSong);
					makePlayer();
				}
			});
		}
		btnPlayStop.setBackground(Color.WHITE);
		btnPlayStop.setBounds(75, 240, 89, 23);
		songPlayer.add(btnPlayStop);
		songPlayer.setVisible(true);
	}
	
	private void makeChanges() {
		this.BPM = (int) bpmSpinner.getValue();
		this.repeat = chckbxRepeat.isSelected();
		
		master.updateSequence(selectedSong, BPM, repeat);
	}
}
