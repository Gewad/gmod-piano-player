package main;

import pianocontroller.Piano;

public class App {

	public App() {
		new Piano();
	}

	public static void main(String[] args) {
		new App();
	}
}