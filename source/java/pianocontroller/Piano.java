package pianocontroller;

import ui.Window;

public class Piano {
	private boolean playing = false;
	private boolean stopped = false;
	private FileProcessor fp;
	private Window ui;
	
	public Piano() {
		fp = new FileProcessor(this);
		ui = new Window(this);
	}
	
	public void play(String sequence) {
		this.playing = true;
		this.stopped = false;
		fp.play(sequence);
	}
	
	public void stop() {
		stopped = true;
	}
	
	protected boolean isStopped() {
		return stopped;
	}
	
	public String[] getSequences() {
		return fp.getSequenceNames();
	}
	
	public String[] getSequenceData(String name) {
		return fp.getSequenceData(name);
	}
	
	public void updateSequence(String name, double BPM, boolean repeat) {
		fp.updateSequence(name, BPM, repeat);
	}
	
	public void saveChanges() {
		fp.saveChanges();
	}
	
	public void setPlaying(boolean playingNew) {
		playing = playingNew;
		ui.makePlayer();
	}
	
	public boolean getPlaying() {
		return playing;
	}
}
