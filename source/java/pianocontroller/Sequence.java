package pianocontroller;

public class Sequence {
	private Note begin;
	
	private String name;
	private double BPM;
	private boolean repeat;
	private String sequenceString;
	private Piano master;

	public Sequence(String name, double BPM, boolean repeat, String seq, Piano master) {
		this.name = name;
		this.BPM = BPM;
		this.repeat = repeat;
		this.sequenceString = seq;
		this.master = master;
	}
	
	private void updateNotes() {
		begin = null;
		double noteLength = (60/this.BPM)*1000;
		String notes[] = this.sequenceString.split(",");
		Note prev = null;
		for(int i = 0; i < notes.length; i++) {
			Note newNote = makeNote(notes[i], noteLength, prev);
			if(this.begin == null) {
				this.begin = newNote;
			} else {
				prev.setNext(newNote);
			}
			prev = newNote;
		}
		if(this.repeat) {
			prev.setNext(this.begin);
		}
	}
	
	private Note makeNote(String note, double noteLength, Note prev) {
		if(note.length() < 2) {
			return new Note(note, noteLength, master);
		} else {
			if (note.charAt(0) == '/') {
				Note current = new Note(Character.toString(note.charAt(1)), noteLength/(note.length()-1), master);
				prev.setNext(current);
				for(int i = 2; i < note.length(); i++) {
					current = new Note(Character.toString(note.charAt(i)), noteLength/(note.length()-1), master);
				}
				return current;
			} else {
				return new Note(note, noteLength, master);
			}
		}
	}
	
	public void play() {
		updateNotes();
		begin.play();
	}
	
	public String getName() {
		return this.name;
	}
	
	public double getBPM() {
		return this.BPM;
	}
	
	public boolean getRepeat() {
		return this.repeat;
	}
	
	public String getSequenceString() {
		return this.sequenceString;
	}
	
	public void setBPM(double BPM) {
		this.BPM = BPM;
	}
	
	public void setRepeat(boolean repeat) {
		this.repeat = repeat;
	}
}
