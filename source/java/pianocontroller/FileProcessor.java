package pianocontroller;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class FileProcessor {
	private File musicFile = new File("music.txt");
	List<Sequence> sequences = new LinkedList<Sequence>();
	private Piano master;
	
	public FileProcessor(Piano piano) {
		master = piano;
		readFile();
	}
	
	private void readFile() {
		Scanner scanner = null;
		try {
			scanner = new Scanner(musicFile);
		} catch (FileNotFoundException e) {
			try {
				musicFile.createNewFile();
				scanner = new Scanner(musicFile);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		while(scanner.hasNextLine()) {
			String parts[] = scanner.nextLine().split(";");
			String dataParts[] = parts[0].split(",");
			double bpm =(double) Integer.parseInt(dataParts[1]);
			boolean repeat = Boolean.parseBoolean(dataParts[2]);
			sequences.add(new Sequence(dataParts[0], bpm, repeat, parts[1], master));
		}
		
		scanner.close();
	}
	
	public void play(String name) {
		getSequence(name).play();
	}
	
	private Sequence getSequence(String name) {
		for(int i = 0; i < sequences.size(); i++) {
			if(sequences.get(i).getName().equals(name)) {
				return sequences.get(i);
			}
		}
		return null;
	}
	
	public String[] getSequenceNames() {
		String names[] = new String[sequences.size()];
		for(int i = 0; i < sequences.size(); i++) {
			names[i] = sequences.get(i).getName();
		}
		return names;
	}
	
	public String[] getSequenceData(String name) {
		Sequence seq = getSequence(name);
		if(seq == null) {
			return null;
		}
		
		String data[] = {seq.getName(), Double.toString(seq.getBPM()), Boolean.toString(seq.getRepeat())};
		return data;
	}
	
	public void updateSequence(String name, double BPM, boolean repeat) {
		Sequence seq = getSequence(name);
		if(seq == null) {
			return;
		}
		
		seq.setBPM(BPM);
		seq.setRepeat(repeat);
	}

	public void saveChanges() {
		FileWriter fw = null;
		try{    
			fw = new FileWriter(this.musicFile);
			for(int i = 0; i < sequences.size(); i++) {
				Sequence seq = sequences.get(i);
				System.out.println(seq.getName() + "," + seq.getBPM() + "," + seq.getRepeat() + ";" + seq.getSequenceString());
				fw.write(seq.getName() + "," + ((int) seq.getBPM()) + "," + seq.getRepeat() + ";" + seq.getSequenceString() + "\n");
			}
		} catch(Exception e) {System.out.println(e);}
		try {
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
