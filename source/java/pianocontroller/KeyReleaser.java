package pianocontroller;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.TimerTask;

public class KeyReleaser extends TimerTask {
	
	private String keys;
	private Note next = null;
	private Piano master;
	
	public KeyReleaser(String keys, Note next, Piano master) {
		this.keys = keys;
		this.next = next;
		this.master = master;
	}
	
	public void run() {
		Robot robot;
		try {
			robot = new Robot();
			for(int i = 0; i < keys.length(); i++) {
				System.out.println("Releasing: " + keys.charAt(i));
				robot.keyRelease((int) keys.toCharArray()[i]);
			}
			robot.keyRelease(KeyEvent.VK_SHIFT);
			Thread.sleep(10);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(next != null && !this.master.isStopped()) {
			next.play();
		} else {
			this.master.setPlaying(false);
		}
	}

}
