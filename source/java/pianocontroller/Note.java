package pianocontroller;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Timer;
import java.util.TimerTask;

public class Note {
	private String pressedKeys = "";
	private String keys = "";
	private long time = 0;
	private Note next = null;
	private Piano master;
	
	public Note(String keys, double timePressed, Piano master) {
		this.keys = keys;
		this.time =(long) timePressed;
		this.master = master;
	}
	
	public void play() {
		pressKeys(this.keys);
		
		// Set timer to release buttons in time.
		Timer timer = new Timer(true);
		TimerTask timerTask = new KeyReleaser(this.pressedKeys, this.next, this.master);
		timer.schedule(timerTask, (this.time-10));
	}
	
	private void pressKeys(String keys) {
		char keyChars[] = keys.toCharArray();
		for(int i = 0; i < keys.length(); i++) {
			if(Character.isDigit(keyChars[i])) {
				this.pressKey(keyChars[i], false);
			} else if (Character.isLetter(keyChars[i])) {
				if(Character.isUpperCase(keyChars[i])) {
					pressKey(keyChars[i], true);
				} else {
					pressKey(Character.toUpperCase(keyChars[i]), false);
				}
			} else {
				pressKey(specialToNumber(keyChars[i]), true);
			}
		}
	}
	
	private void pressKey(char key, boolean capitalised) {
		System.out.println("Pressing: " + key + " - " + capitalised);
		try {
			Robot robot = new Robot();
			if(capitalised) {
				robot.keyPress(KeyEvent.VK_SHIFT);
			}
			robot.keyPress(key);
			this.pressedKeys += key;
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private char specialToNumber(char character) {
		switch(character) {
			case '!': return '1';
			case '@': return '2';
			case '$': return '4';
			case '%': return '5';
			case '^': return '6';
			case '*': return '8';
			case '(': return '9';
		}
		return ',';
	}
	
	public void setNext(Note next) {
		this.next = next;
	}
}
